package com.staysanta.somevaluablerest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class SomeValuableRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SomeValuableRestApplication.class, args);
    }
}
