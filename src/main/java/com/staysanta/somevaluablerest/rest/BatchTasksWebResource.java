package com.staysanta.somevaluablerest.rest;

import com.google.common.collect.Lists;
import com.staysansata.somevaluableworker.api.TaskRequest;
import com.staysansata.somevaluableworker.api.TaskRunRequest;
import com.staysanta.somevaluablerest.domain.Task;
import com.staysanta.somevaluablerest.domain.TaskRepository;
import com.staysanta.somevaluablerest.rest.dto.TaskRequestPlaced;
import com.staysanta.somevaluablerest.streaming.TaskRequestPublisher;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.UUID;

@Named
@Singleton
@Path("/batch")
@Consumes(MediaType.MULTIPART_FORM_DATA)
@Produces(MediaType.APPLICATION_JSON)
public class BatchTasksWebResource {
    private static final Logger LOG = LoggerFactory.getLogger(BatchTasksWebResource.class);

    private final TaskRepository taskRepository;

    private final TaskRequestPublisher publisher;

    @Inject
    public BatchTasksWebResource(TaskRepository taskRepository, TaskRequestPublisher publisher) {
        this.taskRepository = taskRepository;
        this.publisher = publisher;
    }

    @PUT
    public Response putBatchTaskRequests(@Context HttpServletRequest httpRequest,
                                         @FormDataParam("file") InputStream uploadedInputStream,
                                         @FormDataParam("file") FormDataContentDisposition fileDetail,
                                         @FormDataParam("algo") String algorithm) throws IOException {
        final UUID userId = (UUID) httpRequest.getSession().getAttribute(UserIdFilter.USER_ID);

        BufferedReader bufferedReader = null;
        try {
            List<TaskRequestPlaced> batchResponse = Lists.newArrayList();
            bufferedReader = new BufferedReader((new InputStreamReader(uploadedInputStream)));
            bufferedReader.lines().forEachOrdered(url -> {
                LOG.debug("User {} requested in batch to start a task using {} algorithm for url {}", userId, algorithm, url);

                Task task = new Task();
                task.setUserId(userId);
                task.setSrc(url);
                taskRepository.save(task);

                TaskRequest taskRequest = new TaskRunRequest(task.getId(), task.getSrc(), TaskRunRequest.Algorithm.MD5); //TODO
                publisher.publish(taskRequest);

                batchResponse.add(new TaskRequestPlaced(userId, task.getId(), task.getSrc()));
            });
            return Response.ok().entity(batchResponse).build();
        } finally {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        }
    }
}
