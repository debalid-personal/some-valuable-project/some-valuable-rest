package com.staysanta.somevaluablerest.rest;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JerseyConfig {

    @Bean("jerseyResourceConfiguration")
    public ResourceConfig configure() {
        ResourceConfig resourceConfig = new ResourceConfig();

        resourceConfig.property(ServerProperties.RESPONSE_SET_STATUS_OVER_SEND_ERROR, true);

        resourceConfig.register(TaskWebResource.class);
        resourceConfig.register(BatchTasksWebResource.class);
        resourceConfig.register(UserIdFilter.class);

        resourceConfig.register(MultiPartFeature.class);

        return resourceConfig;
    }
}
