package com.staysanta.somevaluablerest.rest;

import com.staysansata.somevaluableworker.api.TaskCancelRequest;
import com.staysansata.somevaluableworker.api.TaskRequest;
import com.staysansata.somevaluableworker.api.TaskRunRequest;
import com.staysanta.somevaluablerest.domain.Task;
import com.staysanta.somevaluablerest.domain.TaskRepository;
import com.staysanta.somevaluablerest.rest.dto.*;
import com.staysanta.somevaluablerest.streaming.TaskRequestPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Singleton
@Path("/tasks")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TaskWebResource {
    private static final Logger LOG = LoggerFactory.getLogger(TaskWebResource.class);

    private final TaskRequestPublisher publisher;
    private final TaskRepository taskRepository;
    private final TaskViewConverter converter;

    @Inject
    public TaskWebResource(TaskRequestPublisher publisher, TaskRepository taskRepository, TaskViewConverter converter) {
        this.publisher = publisher;
        this.taskRepository = taskRepository;
        this.converter = converter;
    }

    @PUT
    public Response addTask(@Context HttpServletRequest httpRequest, TaskRunInput taskRunInput) {
        final UUID userId = (UUID) httpRequest.getSession().getAttribute(UserIdFilter.USER_ID);
        LOG.debug("User {} requested to start following: {}", userId, taskRunInput);

        Task task = new Task();
        task.setUserId(userId);
        task.setSrc(taskRunInput.getSource());
        taskRepository.save(task);

        TaskRequest taskRequest = new TaskRunRequest(task.getId(), taskRunInput.getSource(), taskRunInput.getAlgorithm());
        publisher.publish(taskRequest);
        return Response.created(UriBuilder.fromResource(TaskWebResource.class).path(TaskWebResource.class, "getOneTask").build(task.getId()))
                .entity(new TaskRequestPlaced(userId, task.getId(), task.getSrc()))
                .build();
    }

    @GET
    public Collection<TaskView> getAllTasks() {
        return taskRepository.findAll().stream().map(converter::convertToView).collect(Collectors.toList());
    }

    @GET
    @Path("/{taskId}")
    public Optional<TaskView> getOneTask(@PathParam("taskId") UUID taskId) {
        return Optional.ofNullable(taskRepository.findOne(taskId)).map(converter::convertToView);
    }

    @PUT
    @Path("/cancel")
    public Response cancelTask(@Context HttpServletRequest httpRequest, TaskCancelInput cancelInput) {
        final UUID userId = (UUID) httpRequest.getSession().getAttribute(UserIdFilter.USER_ID);
        LOG.debug("User {} requested to cancel following: {}", userId, cancelInput);


        final Optional<Task> task = Optional.ofNullable(taskRepository.findOne(cancelInput.getTaskId()));
        if (task.isPresent() && Task.ALLOWED_TO_CANCEL.contains(task.get().getStatus())) {
            TaskRequest taskRequest = new TaskCancelRequest(cancelInput.getTaskId());
            publisher.publish(taskRequest);
            return Response.ok(new TaskRequestPlaced(userId, taskRequest.getId(), task.get().getSrc())).build();
        }
        return Response.status(Response.Status.BAD_REQUEST).build();
    }
}
