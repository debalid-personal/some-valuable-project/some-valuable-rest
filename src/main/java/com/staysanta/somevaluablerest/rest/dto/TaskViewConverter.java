package com.staysanta.somevaluablerest.rest.dto;

import com.google.common.collect.Sets;
import com.staysanta.somevaluablerest.domain.Task;

import javax.inject.Named;
import javax.inject.Singleton;
import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;

@Singleton
@Named
public class TaskViewConverter {
    private static final HashSet<Task.Status> FINISHED_STATUSES = Sets.newHashSet(Task.Status.DONE, Task.Status.FAILED, Task.Status.CANCELED);

    public TaskView convertToView(Task domainEntity) {
        long seconds = 0;
        if (Task.Status.STARTED == domainEntity.getStatus()) {
            seconds = Duration.between(Instant.now(), domainEntity.getStartedAt().toInstant()).abs().getSeconds();
        } else if (FINISHED_STATUSES.contains(domainEntity.getStatus())) {
            seconds = Duration.between(domainEntity.getFinishedAt().toInstant(), domainEntity.getStartedAt().toInstant()).abs().getSeconds();
        }
        return TaskView.builder()
                .setId(domainEntity.getId())
                .setUserId(domainEntity.getUserId())
                .setMessage(domainEntity.getResult())
                .setStatus(domainEntity.getStatus().toString())
                .setSeconds(seconds)
                .setSource(domainEntity.getSrc())
                .build();
    }
}
