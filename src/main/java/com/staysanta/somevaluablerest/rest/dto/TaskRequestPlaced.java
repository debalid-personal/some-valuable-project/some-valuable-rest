package com.staysanta.somevaluablerest.rest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.UUID;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TaskRequestPlaced {
    private final UUID userId;
    private final UUID taskId;
    private final String src;

    public TaskRequestPlaced(UUID userId, UUID taskId, String src) {
        this.userId = userId;
        this.taskId = taskId;
        this.src = src;
    }

    @JsonProperty("userId")
    public UUID getUserId() {
        return userId;
    }

    @JsonProperty("taskId")
    public UUID getTaskId() {
        return taskId;
    }

    @JsonProperty("src")
    public String getSrc() {
        return src;
    }
}
