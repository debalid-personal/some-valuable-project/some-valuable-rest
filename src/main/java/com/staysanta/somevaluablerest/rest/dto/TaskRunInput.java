package com.staysanta.somevaluablerest.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.staysansata.somevaluableworker.api.TaskRunRequest;

@JsonInclude(JsonInclude.Include.NON_NULL)
public final class TaskRunInput {
    private final String source;
    private final TaskRunRequest.Algorithm algorithm;

    @JsonCreator
    public TaskRunInput(@JsonProperty("src") String source, @JsonProperty("algo") TaskRunRequest.Algorithm algorithm) {
        this.source = source;
        this.algorithm = algorithm;
    }

    @JsonProperty("src")
    public String getSource() {
        return source;
    }

    @JsonProperty("algo")
    public TaskRunRequest.Algorithm getAlgorithm() {
        return algorithm;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("source", source)
                .add("algorithm", algorithm)
                .toString();
    }
}
