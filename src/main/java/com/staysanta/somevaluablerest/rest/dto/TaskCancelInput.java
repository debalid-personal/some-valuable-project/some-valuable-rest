package com.staysanta.somevaluablerest.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.MoreObjects;
import com.staysansata.somevaluableworker.api.TaskRunRequest;

import java.util.UUID;

@JsonInclude(JsonInclude.Include.NON_NULL)
public final class TaskCancelInput {
    private final UUID taskId;

    @JsonCreator
    public TaskCancelInput(@JsonProperty("taskId") UUID taskId) {
        this.taskId = taskId;
    }

    public UUID getTaskId() {
        return taskId;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("taskId", taskId)
                .toString();
    }
}
