package com.staysanta.somevaluablerest.rest.dto;

import java.util.UUID;

public class TaskView {
    private final UUID id;
    private final UUID userId;
    private final String status;
    private final String message;
    private final String src;
    private final Long seconds;

    private TaskView(UUID id, UUID userId, String status, String message, String src, Long seconds) {
        this.id = id;
        this.userId = userId;
        this.status = status;
        this.message = message;
        this.src = src;
        this.seconds = seconds;
    }

    public UUID getId() {
        return id;
    }

    public UUID getUserId() {
        return userId;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Long getSeconds() {
        return seconds;
    }

    public String getSrc() {
        return src;
    }

    public static Builder builder() {
        return new Builder();
    }

    protected static class Builder {
        private UUID id;
        private UUID userId;
        private String status;
        private String message;
        private Long seconds;
        private String src;

        public Builder setId(UUID id) {
            this.id = id;
            return this;
        }

        public Builder setUserId(UUID userId) {
            this.userId = userId;
            return this;
        }

        public Builder setStatus(String status) {
            this.status = status;
            return this;
        }

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setSeconds(Long seconds) {
            this.seconds = seconds;
            return this;
        }

        public Builder setSource(String src) {
            this.src = src;
            return this;
        }

        public TaskView build() {
            return new TaskView(id, userId, status, message, src, seconds);
        }
    }
}
