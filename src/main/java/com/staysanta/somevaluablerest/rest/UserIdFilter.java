package com.staysanta.somevaluablerest.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.UUID;

@Provider
public class UserIdFilter implements ContainerRequestFilter{
    public static final String USER_ID = "userId";

    @Context
    private HttpServletRequest request;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        final HttpSession session = request.getSession(true);
        UUID userId = (UUID) session.getAttribute(USER_ID);
        if (userId == null) {
            userId = UUID.randomUUID();
            session.setAttribute(USER_ID, userId);
        }
    }
}
