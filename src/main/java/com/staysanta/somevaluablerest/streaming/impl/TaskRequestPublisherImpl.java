package com.staysanta.somevaluablerest.streaming.impl;

import com.staysansata.somevaluableworker.api.TaskRequest;
import com.staysanta.somevaluablerest.streaming.TaskRequestPublisher;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Named
@Singleton
public class TaskRequestPublisherImpl implements TaskRequestPublisher {

    private final Source source;

    @Inject
    public TaskRequestPublisherImpl(Source source) {
        this.source = source;
    }

    @Override
    public void publish(TaskRequest request) {
        source.output().send(MessageBuilder.withPayload(request).build());
    }
}
