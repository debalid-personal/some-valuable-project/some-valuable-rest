package com.staysanta.somevaluablerest.streaming.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.staysansata.somevaluableworker.api.*;
import com.staysanta.somevaluablerest.domain.Task;
import com.staysanta.somevaluablerest.domain.TaskRepository;
import com.staysanta.somevaluablerest.streaming.TaskNotificationsListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Named
@Singleton
public class TaskNotificationListenerImpl implements TaskNotificationsListener {
    private static final Logger LOG = LoggerFactory.getLogger(TaskNotificationsListener.class);

    private final TaskRepository taskRepository;

    private final ObjectMapper mapper;

    @Inject
    public TaskNotificationListenerImpl(TaskRepository taskRepository, ObjectMapper mapper) {
        this.taskRepository = taskRepository;
        this.mapper = mapper;
    }

    @Override
    @ServiceActivator(inputChannel = Sink.INPUT)
    public void onUpdate(Message<byte[]> message) {
        try {
            TaskNotification notification = mapper.readValue(message.getPayload(), TaskNotification.class);
            LOG.debug("Received {}", notification);
            notification.acceptVisitor(new NotificationVisitor());
        } catch (IOException e) {
            LOG.error("Unexpected error", e);
        }
    }

    /**
     * One assumption is that incoming queue should guarantee ordered delivery, and all notifications about the same task id
     * must be in the same partition.
     */
    private final class NotificationVisitor implements TaskNotificationVisitor {
        @Override
        public void visitTaskStarted(TaskStarted notification) {
            Optional.ofNullable(taskRepository.findOne(notification.getId())).ifPresent(x -> {
                x.setStartedAt(notification.getStartedAt());
                x.setStatus(Task.Status.STARTED);
                taskRepository.save(x);
            });
        }

        @Override
        public void visitTaskCanceled(TaskCanceled notification) {
            Optional.ofNullable(taskRepository.findOne(notification.getId())).ifPresent(x -> {
                Preconditions.checkArgument(Task.ALLOWED_TO_CANCEL.contains(x.getStatus()), "Impossible to cancel an active operation");
                x.setFinishedAt(notification.getCanceledAt());
                x.setStatus(Task.Status.CANCELED);
                taskRepository.save(x);
            });
        }

        @Override
        public void visitTaskDone(TaskDone notification) {
            Optional.ofNullable(taskRepository.findOne(notification.getId())).ifPresent(x -> {
                x.setFinishedAt(notification.getFinishedAt());
                x.setStatus(Task.Status.DONE);
                x.setResult(notification.getDigest());
                taskRepository.save(x);
            });
        }

        @Override
        public void visitTaskFailed(TaskFailed notification) {
            Optional.ofNullable(taskRepository.findOne(notification.getId())).ifPresent(x -> {
                x.setFinishedAt(notification.getFinishedAt());
                x.setStatus(Task.Status.FAILED);
                x.setResult(notification.getError());
                taskRepository.save(x);
            });
        }
    }
}
