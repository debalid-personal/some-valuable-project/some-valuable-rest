package com.staysanta.somevaluablerest.streaming;

import org.springframework.messaging.Message;

public interface TaskNotificationsListener {
    void onUpdate(Message<byte[]> message);
}
