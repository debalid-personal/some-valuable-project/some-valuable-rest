package com.staysanta.somevaluablerest.streaming;

import com.staysansata.somevaluableworker.api.TaskRequest;

public interface TaskRequestPublisher {
    /**
     * Publishes a given task request to work queue
     */
    void publish(TaskRequest request);
}
