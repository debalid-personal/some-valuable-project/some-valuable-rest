package com.staysanta.somevaluablerest.domain;

import com.google.common.base.MoreObjects;
import com.google.common.collect.Sets;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.UUID;

@Document
public class Task {
    public static final HashSet<Status> ALLOWED_TO_CANCEL = Sets.newHashSet(Task.Status.PENDING, Task.Status.STARTED);

    public enum Status {
        PENDING, STARTED, CANCELED, DONE, FAILED
    }

    @Id
    private final UUID id;

    private UUID userId;

    private Status status;

    private ZonedDateTime createdAt;

    private ZonedDateTime startedAt;

    private ZonedDateTime finishedAt;

    private String result;

    private String src;

    public Task() {
        this.id = UUID.randomUUID();
        this.status = Status.PENDING;
        this.createdAt = ZonedDateTime.now();
    }

    public UUID getId() {
        return id;
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ZonedDateTime getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(ZonedDateTime startedAt) {
        this.startedAt = startedAt;
    }

    public ZonedDateTime getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(ZonedDateTime finishedAt) {
        this.finishedAt = finishedAt;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("userId", userId)
                .add("status", status)
                .add("createdAt", createdAt)
                .add("startedAt", startedAt)
                .add("finishedAt", finishedAt)
                .add("result", result)
                .add("src", src)
                .toString();
    }
}
