FROM openjdk:alpine
ARG JAR_NAME
ADD ${JAR_NAME} app.jar
ENV JAVA_OPTS="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=14000"
EXPOSE 14000
ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -jar app.jar"]
